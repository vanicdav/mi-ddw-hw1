package cz.ctu.fit.ddw;

import java.io.FileWriter;
import org.apache.commons.lang.ArrayUtils;

/**
 *
 * @author David
 */
public class Crawler {

    public static String SERVER = "http://www.edownload.cz/";
    public static String ROOT = "/";
    public static String ASSIGN = "=";
    public static String FREEWARE = "free";
    public static String SW_SECTION = "/sw/";
    public static String PRODUCT_LIST = "<div class=\"vypis\">";
    public static String DISCUSSION = "komentare/";
    
    private String[] categories;
    private String[] products;
    private int pages;
    private String pagination;
    
    public void crawl () {
	categories = crawlHomepage();
	for (String cat : categories) {
	    if (cat == null) {
		continue;
	    }
	    pages = 0;
//if (cat.indexOf("kancelarske-programy")>0){
	    products = crawlCategory(cat);
	    if (pages > 1) {
		for (int i = 2; i <= pages; i++) {
		    products = (String[]) ArrayUtils.addAll(products, crawlCategory(pagination + i));
		}
	    }
	    log (cat.substring(cat.indexOf(ROOT) + 1, cat.lastIndexOf(ROOT)), products);
	    for (String prg : products) {
		if (prg == null) {
		    continue;
		}
		crawlProgram(prg);
	    }
//}
	}
    }
    
    private Boolean log (String file, String[] text) {
	try {
	    FileWriter writer = new FileWriter(GATEEmbedded.LOG_PREFIX + file + GATEEmbedded.LOG_SUFFIX, true);
	    for (int i = 0; i < text.length; i++) {
		writer.write(text[i] + "\r\n");
	    }
	    writer.close();
	    return true;
	} catch (Exception e) {
	    System.out.println(e);
	}
	return false;
    }
    
    private String[] crawlHomepage () {
	String[] links = HTML.extractLinks(HTML.getSourceCode(SERVER));
	int index = 0;
	for (int i = 1; i < links.length; i++) {
	    int flag = 1;
	    if (links[i].substring(0, ROOT.length()).compareTo(ROOT) != 0) {
		// get rid of out of server links
		flag = 0;
	    } else if (links[i].length() < SW_SECTION.length() || links[i].substring(0, SW_SECTION.length()).compareTo(SW_SECTION) == 0){
		// get rid of featured links
		flag = 0;
	    } else if (links[i].indexOf("?") > 0 || links[i].indexOf(":") > 0) {
		// get rid of search & absolute links
		flag = 0;
	    } else {
		links[i] = links[i].substring(ROOT.length());
		if (links[i].indexOf(ROOT) + 1 == links[i].length()) {
		    // get rid of top level categories
		    flag = 0;
		} else {
		    index++;
		}
	    }
	    if (flag == 0) {
		links[i] = "";
	    }
	}
	return HTML.copyValidValues(links, index);
    }
    
    private String[] crawlCategory ( String url ) {
	String code = HTML.getSourceCode(SERVER + url);
	String[] programs = code.split(PRODUCT_LIST);
	int index = 0;
	for (int i = 1; i < programs.length; i++) {
	    if (pages == 0 && i + 1 == programs.length) {
		getPagination(programs[i], url);
	    }
	    if (programs[i].indexOf(FREEWARE) < 0) {
		// work only w/ non-free SW
		String[] product = HTML.extractLinks(programs[i]);
		programs[i] = product[0];
		index++;
	    } else {
		programs[i] = "";
	    }
	}
	return HTML.copyValidValues(programs, index);
    }
    
    private void getPagination (String current, String url) {
	String[] product = HTML.extractLinks(current);
	String listing = "";
	for (String link : product) {
	    if (link.indexOf(url) >= 0 && (link.length() > listing.length() || link.compareTo(listing) > 0)) {
		listing = link;
	    }
	}
	int pos = listing.lastIndexOf(ASSIGN) + 1;
	pagination = listing.substring(1, pos);
	pages = Integer.parseInt(listing.substring(pos));
    }
    
    private void crawlProgram ( String url ) {
	String code = HTML.getSourceCode(SERVER + url + DISCUSSION);
	String name = "";
	int open = code.indexOf("<h1");
	int close = code.indexOf("</h1");
	if (open > 0 && close > 0) {
	    name = code.substring(open, close);
	    name = " " + name.substring(name.indexOf(">") + 1, name.lastIndexOf("-")).trim();
	}
	GateClient.addDocument ( /*HTML.stripTags (*/ code /*)*/ );
	String[] pirates = GateClient.run();
	String[] tmp = new String[1];
	if ( pirates != null ) {
	    for (String p : pirates) {
		tmp[0] = p + name;
		log (p.substring(0, 1), tmp);
	    }
	}	
    }
    
}
