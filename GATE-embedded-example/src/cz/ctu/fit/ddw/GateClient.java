
package cz.ctu.fit.ddw;

import gate.Annotation;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.ArrayUtils;

/**
 * @author Milan Dojchinovski
 * <milan (at) dojchinovski (dot) mk>
 * Twitter: @m1ci
 * www: http://dojchinovski.mk
 */
public class GateClient {
    
    public static char dot = '.';
    
    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    private static boolean isGatePrepared = false;
    private static Corpus corpus;
    
    public static void addDocument(String doc){
        if(!isGateInitilised){
            initialiseGate();            
        }        
        if(!isGatePrepared){
            prepareGate();            
        }        
        try {
	    corpus.add(Factory.newDocument(doc));
        } catch (GateException ex) {
	    logException(ex);
        }
    }

    private static void logException (GateException ex) {
        Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    public static String[] run() {
        if(!isGateInitilised){
            initialiseGate();            
        }        
        if(!isGatePrepared){
            prepareGate();            
        }        

	try {
	    String[] emails = null;
            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);
	    //run the pipeline
            annotationPipeline.execute();
	    
            // loop through the documents in the corpus
            for ( int i = 0; i < corpus.size(); i++ ) {

                Document doc = corpus.get(i);
                FeatureMap futureMap = null;
                // get all Token annotations
                ArrayList tokenAnnotations = new ArrayList(doc.getAnnotations().get("Email", futureMap));
		String[] tokens = new String[tokenAnnotations.size()];
		int index = 0;
                // loop through the Token annotations
                for(int j = 0; j < tokenAnnotations.size(); ++j) {
                    Annotation token = (Annotation)tokenAnnotations.get(j);
                    // get the underlying string for the Token
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    tokens[index] = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
		    // is email really valid?
		    if (tokens[index].indexOf(dot + dot) >= 0) {
			index--;
		    } else {
			if (tokens[index].charAt(0) == dot) {
			    tokens[index] = tokens[index].substring(1);
			}
			int pos = tokens[index].length() - 1;
			if (tokens[index].charAt(pos) == dot) {
			    tokens[index] = tokens[index].substring(0, pos);
			}
		    }
		    index++;
		    //System.out.println("Token: " + underlyingString);
                    // get the features of the token
                    //FeatureMap annFM = token.getFeatures();
                    // get the value of the "string" feature
                    //String value = (String)annFM.get((Object)"string");
                    //System.out.println("Token: " + value);
                }
		if (index != tokenAnnotations.size()) {
		    String[] tmp = new String[index];
		    System.arraycopy(tokens, 0, tmp, 0, index);
		    tokens = tmp;
		}
		emails = (String[]) ArrayUtils.addAll(emails, tokens);
            }
	    
	    corpus.clear();
	    
	    return emails;
	} catch (Exception e) {
	    System.out.println(e);
	}
	return null;
    }

    private static void prepareGate () {
        try {                
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            ProcessingResource splitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            ProcessingResource gazetteerPR = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
            
            // locate the JAPE grammar file
            File japeOrigFile = new File(GATEEmbedded.LOG_PREFIX + GATEEmbedded.GRAMMAR_NAME + ".jape");
            java.net.URI japeURI = japeOrigFile.toURI();
            
            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }
            
            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(splitterPR);
            annotationPipeline.add(gazetteerPR);
            annotationPipeline.add(japeTransducerPR);
            
            corpus = Factory.newCorpus("");
            isGatePrepared = true;
        } catch (GateException ex) {
	    logException(ex);
        }
    
    }
    
    private static void initialiseGate() {
        
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File(GATEEmbedded.GATE_INSTALL_PATH);
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
	    String slash = "\\";
	    if (GATEEmbedded.GATE_INSTALL_PATH.indexOf("/") >= 0) {
		slash = "/";
	    }
            File pluginsHome = new File(GATEEmbedded.GATE_INSTALL_PATH + slash + "plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File(GATEEmbedded.GATE_INSTALL_PATH, "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
	    logException(ex);
        }
    }    
}