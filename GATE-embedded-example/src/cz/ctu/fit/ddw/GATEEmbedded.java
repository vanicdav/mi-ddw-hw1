
package cz.ctu.fit.ddw;

/**
 * @author Milan Dojchinovski
 * <milan (at) dojchinovski (dot) mk>
 * Twitter: @m1ci
 * www: http://dojchinovski.mk
 */
public class GATEEmbedded {

    /* PARAMETERS */
    public static String GATE_INSTALL_PATH = "C:\\Program Files\\GATE_Developer_7.1";
    public static String LOG_PREFIX = "../"; // also where the grammar file is stored
    public static String GRAMMAR_NAME = "jape-example"; 
    /* /PARAMETERS */

    public static String LOG_SUFFIX = ".txt";

    public static void main(String[] args) {
	Crawler c = new Crawler();
	c.crawl();
    }
    
}
