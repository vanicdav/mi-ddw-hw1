package cz.ctu.fit.ddw;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author David
 */
public class HTML {
    
    public static String open="<";
    public static String close=">";
    
    public static String getSourceCode ( String uri ) {
        /*String sc = "";
        int now;
        try {
            URL url = new URL ( uri );
            URLConnection con = url.openConnection();
            con.connect ();
            InputStream isr = con.getInputStream ();
            while ( ( now = isr.read() ) != -1 ) {
                sc += ( char ) now;
            }
            return sc;
        } catch ( Exception e ) {
            return "";
        }*/
        try {
	    URL url = new URL ( uri );
            URLConnection con = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            String inputLine;
            StringBuilder a = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                a.append(inputLine);
	    }
            in.close();
            return a.toString();
        } catch ( Exception e ) {
            return "";
        }
    }

    public static String[] extractLinks ( String sourceCode ) {
	String[] split = sourceCode.split("<a ");
	int l = split.length;
	String del = "href=";
	String[] links = new String[l - 1];
	for (int i = 1; i < l; i++) {
	    int pos = split[i].indexOf(del) + del.length();
	    char quote = split[i].charAt(pos);
	    links[i-1] = split[i].substring(pos+1);
	    links[i-1] = links[i-1].substring(0, links[i-1].indexOf(quote));
	}
	return links;
    }

    public static String[] copyValidValues (String[] arr, int index) {
	String[] valid = new String[index];
	index = 0;
	for (int i = 1; i < arr.length; i++) {
	    if (arr[i].compareTo("") != 0) {
		valid[index++] = arr[i];
	    }
	}
	return valid;
    }
    
    /*public static String stripTags (String code) {
	String[] content = code.split(open);
	code = "";
	for (int i = 0; i < content.length; i++) {
	    code += content[i].substring(content[i].indexOf(close) + 1).trim() + ". ";
	}
	return code;
    }*/
    
}
